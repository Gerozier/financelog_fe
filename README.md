# FinancelogFrontend

This project is part of the overall FinanceLog project. 

Check out the other repositories here:
- Backend: https://gitlab.com/Gerozier/financelog
- Security: https://gitlab.com/Gerozier/financelog_security

## Inhaltsverzeichnis
1. [GettingStarted](#getting-started)
2. [Contributor](#contributor)
3. [Explanation](#explanation)

<a name="getting-started"></a>
##Getting started
How to get a copy of the Project and run it locally.

### Requirements
To install the Project you need the following Git Repository:
* [Frontend](https://gitlab.com/Gerozier/climbook_fe.git)

### Install
```
cd  c: ./climbook-fe
cd ng serve
```

### Docker
To run the Project as a Docker Container you have to build the image.
```
cd  c: ./climbook-fe
cd docker build -t financelog/ui .
```


<a name="contributor"></a>
## Contributor

- Brämer, Dennis https://gitlab.com/Gerozier
- Schmidt-Berschet, Lisa https://gitlab.com/LisaSila


<a name="explanation"></a>
## Explanation 

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.
