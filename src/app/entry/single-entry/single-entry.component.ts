import { Component, OnInit } from '@angular/core';
import {EntryDTO} from '../entryDTO';
import {EntryService} from '../../services/entry.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-single-entry',
  templateUrl: './single-entry.component.html',
  styleUrls: ['./single-entry.component.css']
})
export class SingleEntryComponent implements OnInit {

  public entryDTO: EntryDTO = {fixCost: false, title: '', value: 0, clientId: ''};

  constructor(private entryService: EntryService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getSingleEntry();
  }

  // macht einen snapshot der URL und nimmt sich die ID raus
  getSingleEntry(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.entryService.getSingleEntry(id).subscribe(response => this.entryDTO = response);
  }

}
