import {Component, OnInit} from '@angular/core';
import {EntryDTO} from './entryDTO';
import {EntryService} from '../services/entry.service';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css']
})
export class EntryComponent implements OnInit {

  public entries: EntryDTO[];

  public entryDTO: EntryDTO = {fixCost: false, title: '', value: 0, clientId: ''};

  constructor(private entryService: EntryService) { }

  ngOnInit() {
    this.entryService.getEntries().subscribe(entries => {
      this.entries = entries;
      console.log(this.entries);
    });
  }

  addEntry() {
    this.entryService
      .createEntry(this.entryDTO)
      .subscribe(entry => this.entries.push(entry));
  }


}
