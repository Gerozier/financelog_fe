export interface EntryDTO {
  title: string;
  fixCost: boolean;
  value: number;
  clientId: string;
}
