import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EntryDTO} from '../entry/entryDTO';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EntryService {

  constructor(private http: HttpClient) { }

  getEntries() {
    return this.http.get<EntryDTO[]>(environment.resource + '/entries');
  }

  getSingleEntry(id: string) {
    return this.http.get<EntryDTO>(environment.resource + '/entry/' + id);
  }

  createEntry(entry: EntryDTO): Observable<EntryDTO> {
    console.log(entry);
    return this.http
      .post<EntryDTO>(environment.resource + '/entries', entry);
  }
}
