import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { EntryComponent } from './entry/entry.component';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import { SingleEntryComponent } from './entry/single-entry/single-entry.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    EntryComponent,
    SingleEntryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
