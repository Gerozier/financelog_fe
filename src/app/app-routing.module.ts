import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EntryComponent} from './entry/entry.component';
import {SingleEntryComponent} from './entry/single-entry/single-entry.component';

const routes: Routes = [
  { path: 'entries', component: EntryComponent },
  { path: 'entry/:id', component: SingleEntryComponent }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
