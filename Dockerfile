FROM node:12.7-alpine AS build
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install -g @angular/cli@9.1.7
COPY . /usr/src/app
CMD ng serve --host 0.0.0.0 --port 4200

